/**
 * We are using the .JS version of an ESLint config file here so that we can
 * add lots of comments to better explain and document the setup.
 */
module.exports = {
    extends: [
        "eslint:recommended",
        "plugin:@typescript-eslint/eslint-recommended",
        "plugin:@typescript-eslint/recommended",
        "plugin:prettier/recommended",
        "plugin:@angular-eslint/recommended",
        "prettier",
    ],
    rules: {
        // ORIGINAL tslint.json -> "directive-selector": [true, "attribute", "app", "camelCase"],
        "@angular-eslint/directive-selector": [
            "error",
            { type: "attribute", prefix: "app", style: "camelCase" },
        ],

        // ORIGINAL tslint.json -> "component-selector": [true, "element", "app", "kebab-case"],
        "@angular-eslint/component-selector": [
            "error",
            { type: "element", prefix: "app", style: "kebab-case" },
        ],
        "prettier/prettier": "error",
        "@typescript-eslint/no-explicit-any": "off",
        "@typescript-eslint/array-type": ["error", { default: "array-simple" }],
        "prefer-template": "error",
        "no-restricted-syntax": "off",
        "@typescript-eslint/no-non-null-assertion": "off",
    },
    overrides: [
        /**
         * This extra piece of configuration is only necessary if you make use of inline
         * templates within Component metadata, e.g.:
         *
         * @Component({
         *  template: `<h1>Hello, World!</h1>`
         * })
         * ...
         *
         * It is not necessary if you only use .html files for templates.
         */
        {
            files: ["*.component.ts"],
            parser: "@typescript-eslint/parser",
            parserOptions: {
                ecmaVersion: 2020,
                sourceType: "module",
            },
            plugins: ["@angular-eslint/template"],
            processor: "@angular-eslint/template/extract-inline-html",
        },
    ],
};

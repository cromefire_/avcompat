import { Component, OnInit } from "@angular/core";

interface SupportResult {
    name: string;
}

enum CodecType {
    VIDEO,
    AUDIO,
}

type Level = "none" | "maybe" | "probably";

interface CodecResult extends SupportResult {
    type: CodecType;
    links: Link[];
    level: Level;
}

interface Mime {
    mime: string;
    supported: boolean;
}

interface ContainerResult extends SupportResult {
    supported: boolean;
    video: boolean;
    audio: boolean;
    codecs: CodecResult[];
    mimes: Mime[];
}

function any(arr: boolean[]): boolean {
    for (const elem of arr) {
        if (elem) {
            return true;
        }
    }
    return false;
}

function compareLevel(a: Level, b: Level): -1 | 0 | 1 {
    switch (a) {
        case "none": {
            switch (b) {
                case "none":
                    return 0;
                case "maybe":
                    return -1;
                case "probably":
                    return -1;
            }
            throw Error("Illegal state");
        }
        case "maybe": {
            switch (b) {
                case "none":
                    return 1;
                case "maybe":
                    return 0;
                case "probably":
                    return -1;
            }
            throw Error("Illegal state");
        }
        case "probably": {
            switch (b) {
                case "none":
                    return 1;
                case "maybe":
                    return 1;
                case "probably":
                    return 0;
            }
            throw Error("Illegal state");
        }
    }
}

interface Link {
    name: string;
    url: string;
}

@Component({
    selector: "app-root",
    templateUrl: "./app.component.html",
    styleUrls: ["./app.component.scss"],
})
export class AppComponent implements OnInit {
    public containers: ContainerResult[] = [];
    public CodecType = CodecType;

    private video = document.createElement("video");
    private audio = document.createElement("audio");

    private static containerSupports(
        container: ContainerResult,
        type: CodecType
    ): boolean {
        switch (type) {
            case CodecType.VIDEO: {
                return container.video;
            }
            case CodecType.AUDIO: {
                return container.audio;
            }
        }
    }

    public ngOnInit(): void {
        // Containers
        this.testContainer("webm", ["video/webm"], true, true);
        this.testContainer(
            "ogg",
            ["application/ogg", "audio/ogg", "video/ogg"],
            true,
            true
        );
        this.testContainer("mp4", ["video/mp4", "audio/mp4"], true, true);

        // Video
        this.testCodec("VP8", "vp8", CodecType.VIDEO, ["webm"]);
        this.testCodec("VP9", "vp9", CodecType.VIDEO, ["webm", "mp4"]);
        this.testCodec("AV1", "av1", CodecType.VIDEO, ["webm"]);
        this.testCodec("Theora", "theora", CodecType.VIDEO, ["ogg"]);
        this.testCodec("Dirac", "dirac", CodecType.VIDEO, ["ogg"]);
        this.testCodec(
            "H264 Baseline",
            "avc1.42E01E",
            CodecType.VIDEO,
            ["mp4"],
            []
        );

        // Audio
        this.testCodec(
            "Vorbis",
            "vorbis",
            CodecType.AUDIO,
            ["ogg", "webm"],
            []
        );
        this.testCodec("Speex", "speex", CodecType.AUDIO, ["ogg"], []);
        this.testCodec("Opus", "opus", CodecType.AUDIO, ["ogg", "webm"], []);
        this.testCodec("Flac", "flac", CodecType.AUDIO, ["ogg"], []);
        this.testCodec("AAC", "mp4a.40.2", CodecType.AUDIO, ["mp4"], []);
    }

    public videoCodecs(): CodecResult[] {
        return this.filterCodecs(CodecType.VIDEO);
    }

    public audioCodecs(): CodecResult[] {
        return this.filterCodecs(CodecType.AUDIO);
    }

    public filterCodecs(type: CodecType): CodecResult[] {
        const all = this.containers
            .filter((c) => {
                switch (type) {
                    case CodecType.VIDEO: {
                        return c.video;
                    }
                    case CodecType.AUDIO: {
                        return c.audio;
                    }
                }
            })
            .map((c) => c.codecs)
            .flat(1)
            .filter((c) => c.type === type);
        const res: Map<string, CodecResult> = new Map();
        for (const cnt of all) {
            if (
                !res.get(cnt.level) ||
                compareLevel(cnt.level, res.get(cnt.level)!!.level) > 0
            ) {
                res.set(cnt.name, cnt);
            }
        }
        const list: CodecResult[] = [];
        for (const e of res.entries()) {
            list.push(e[1]);
        }
        return list;
    }

    private testContainer(
        container: string,
        mimes: string[],
        video: boolean,
        audio: boolean
    ): void {
        const res: Mime[] = [];
        for (const mime of mimes) {
            res.push({
                mime,
                supported: !!this.video.canPlayType(mime),
            });
        }

        this.containers.push({
            name: container,
            video,
            audio,
            supported: any(res.map((m) => m.supported)),
            mimes: res,
            codecs: [],
        });
    }

    private testCodec(
        name: string,
        codec: string,
        type: CodecType,
        containers: string[],
        links: Link[] = []
    ): void {
        for (const container of containers) {
            const cnts = this.containers.filter(
                (cnt) => cnt.name === container
            );
            if (!cnts.length) {
                throw Error(`Container not tested: "${container}"`);
            } else if (cnts.length > 1) {
                throw Error(`Container added multiple times: "${container}"`);
            }

            const cnt = cnts[0];
            if (!AppComponent.containerSupports(cnt, type)) {
                throw Error(
                    `Container not usable for codec type: "${container}", "${codec}"`
                );
            }

            const res: CanPlayTypeResult[] = [];
            for (const mime of cnt.mimes) {
                if (mime.supported) {
                    const test = `${mime.mime};codecs=${codec}`;
                    console.info(`Testing "${test}"`);
                    switch (type) {
                        case CodecType.VIDEO: {
                            res.push(this.video.canPlayType(test));
                            break;
                        }
                        case CodecType.AUDIO: {
                            res.push(this.audio.canPlayType(test));
                            break;
                        }
                    }
                }
            }
            const codecResult: CodecResult = {
                name,
                type,
                level: res.includes("probably")
                    ? "probably"
                    : res.includes("maybe")
                    ? "maybe"
                    : "none",
                links,
            };
            cnt.codecs.push(codecResult);
        }
    }
}

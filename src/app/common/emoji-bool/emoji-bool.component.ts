import { Component, Input } from "@angular/core";

@Component({
    selector: "app-emoji-bool",
    templateUrl: "./emoji-bool.component.html",
    styleUrls: ["./emoji-bool.component.scss"],
})
export class EmojiBoolComponent {
    @Input() bool: boolean;
}

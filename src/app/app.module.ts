import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { ServiceWorkerModule, SwUpdate } from "@angular/service-worker";
import { environment } from "../environments/environment";
import { MatListModule } from "@angular/material/list";
import { MatCardModule } from "@angular/material/card";
import { EmojiBoolComponent } from "./common/emoji-bool/emoji-bool.component";
import { MatToolbarModule } from "@angular/material/toolbar";
import { MatSnackBar, MatSnackBarModule } from "@angular/material/snack-bar";

@NgModule({
    declarations: [AppComponent, EmojiBoolComponent],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ServiceWorkerModule.register("ngsw-worker.js", {
            enabled: environment.production,
        }),
        MatListModule,
        MatCardModule,
        MatToolbarModule,
        MatSnackBarModule,
    ],
    providers: [],
    bootstrap: [AppComponent],
})
export class AppModule {
    constructor(updates: SwUpdate, snackBar: MatSnackBar) {
        updates.available.subscribe((event) => {
            console.log("current version is", event.current);
            console.log("available version is", event.available);
            snackBar
                .open(
                    "New version available, you have to reload to update",
                    "Reload",
                    {
                        duration: 10_000,
                    }
                )
                .onAction()
                .subscribe(() => {
                    location.reload();
                });
        });
    }
}
